create database Examen
use Examen

create table cliente(

	 Codigo_Cliente  int  identity (1,1)  primary key  ,
	Nombre varchar(80),  
	Membres�a varchar(80)
	--;// (Normal, Gold)
	)
	go
	create table video (

Codigo_Video  int  identity (1,1)  primary key  ,
nombre varchar(80),
--precio numeric (12,2),
tipo varchar(80)
)
go

create table renta (

Folio  int  identity (1,1)  primary key  ,
Fecha date,
Codigo_Video  int foreign key  (Codigo_Video) references video(Codigo_Video) ,
Codigo_Cliente  int foreign key  (Codigo_Cliente) references cliente(Codigo_Cliente),
--Subtotal numeric (12,2),
Total numeric (12,2)
)

create view vw_DetalleRenta as
select r.folio, r.fecha, pelicula=v.nombre,v.tipo, cliente=c.Nombre,c.Membres�a, r.Total
from renta r
inner join cliente c on c.Codigo_Cliente=r.Codigo_Cliente
inner join video v on v.Codigo_Video=r.Codigo_Video
